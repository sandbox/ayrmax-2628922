<?php

/**
 * @file
 * Hooks provided by the Callback module.
 */

/**
 * Provide selectable callbacks for the callback field.
 * The callback function is expected to return a render array.
 */
function hook_callback_field_info() {
  return array(
    'node' => array(
      'custom_code' => array(
        'title' => 'Custom code',
        'callback' => 'some_function',
      ),
      'custom_code2' => array(
        'title' => 'Custom code2',
        'callback' => 'some_function2',
      ),
    ),
  );
}
